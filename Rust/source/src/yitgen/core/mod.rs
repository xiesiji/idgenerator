/*
 * 版权属于：yitter(yitter@126.com)
 * 开源地址：https://gitee.com/yitter/idgenerator
 */
mod snow_worker_m1;
mod snow_worker_m2;

pub use snow_worker_m1::SnowWorkerM1;
pub use snow_worker_m2::SnowWorkerM2;

